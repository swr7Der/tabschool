var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

var db = "";

$(document).ready( function() {
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var storage = window.localStorage;
	
	if (storage.getItem("FirstNotesAddition") == '1') {
			db.transaction(makeDataWithDB, errorCB, successCB);
	}
	
	if (storage.getItem("FirstNotesAddition") == '0') {
			refreshDB();
	}
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
	
	var userId = JSON.parse(storage.getItem("signInJSON")).id;
	
	//alert(userId);
	
	var itemsForSelection = storage.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
		
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}
	
});

function addNoteBook() {
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var emailId = receivedDataJSON.email;
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Adding Notebook...", true);
		
		//alert("1"+emailId+noteBookName);
		//alert(storage.getItem("NotebookNames"));
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotebook",
        data: {"email_id": emailId, "name":noteBookName},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        		
        		storage.setItem("FirstNotebookAddition","0");
        	
        		alert(response.msg);
        		refreshDBNoteBook(emailId);
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});		
	
	}
}

function refreshDBNoteBook(emailId) {
	
	//alert("1"+emailId);
	window.plugins.spinnerDialog.show(null,"Getting Data...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/listnotebook",
        data: {"email_id": emailId},
        success: function (response) {
        		var nbname = response.note_book_data;
        		window.plugins.spinnerDialog.hide();
        		
        		//alert(JSON.stringify(nbname));
        		
        		storage.setItem("NotebookNames","");
        		
        		for(var i=0; i<nbname.length; i++){
					var JSONValue = nbname[i].name; 
					storage.setItem("NotebookNames",storage.getItem("NotebookNames")+JSONValue+","); 
					//alert(storage.getItem("NotebookNames"));      		
        		
        		}
        		
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add a notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val();
	var notesOption = $('#notes_option').find(":selected").text();
	var notesValue = $("#textarea1").val();
	
	//alert(notesTitle+notesOption+notesValue);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Loading Data...", true);
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        		//alert(JSON.stringify(response));
        		alert(response.msg);
        		storage.setItem("FirstNotesAddition","0");
        		alert("You need to fetch the notes for using it offline. Please click on any notebook for this purpose");
        		//refreshDB();
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}

function editNotes(descriptionValue) {
	
	var a = 	unescape(descriptionValue);
	
	//alert(a);
	
	var descriptionUnescaped = JSON.parse(a);
	
	var noteId = descriptionUnescaped.id;
	var descriptionToSet = descriptionUnescaped.description;
	
	//alert(noteId+" "+descriptionToSet);
	
	$("#edit").show();
	
	$("#textarea2").val(descriptionToSet);
	
	document.getElementById("cancel_edit_save").onclick = function (e) { 
		 $("#edit").hide();
	}
	
	
	document.getElementById("edit_save").onclick = function (e) {
		
		$("#edit").hide(); 
	
		var descriptionToSend = $("#textarea2").val();
		
		//alert(noteId+" "+descriptionToSend);	
		window.plugins.spinnerDialog.show(null,"Saving...", true);	
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/updatenotes",
        data: {"noteid": noteId, "description":descriptionToSend},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		storage.setItem("FirstNotesAddition","0");
        		//refreshDB();
        		alert("You need to fetch the notes for using it offline. Please click on any notebook for this purpose");
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	}); 
	}
	
	
}

function deleteNotes(id) {
	
	//alert(id);
	$("#delete").show();	
	
	document.getElementById("dont_delete").onclick = function (e) { 
		$("#delete").hide(); 
	}	
	
	document.getElementById("delete_now").onclick = function (e) {
		$("#delete").hide();
		
		window.plugins.spinnerDialog.show(null,"Deleting...", true);
			
		$.ajax({
			
			type: 'POST',
			url: 'http://vnenterprizes.in/tabschool/index.php/api/deletenotes',
			data: { "noteid":id },
			
			success: function (response) {
				window.plugins.spinnerDialog.hide();
				alert(response.msg);
				storage.setItem("FirstNotesAddition","0");
				//refreshDB();	
				alert("You need to fetch the notes for using it offline. Please click on any notebook for this purpose");
				window.location = "my-notes.html";
			},
			error: function (errorMessage) {
				window.plugins.spinnerDialog.hide();
				alert(errorMessage);	
			}
			
		});
	}
	
}

function setValue(result) {
	return unescape(result);	
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}


function refreshDB() {

	db.transaction(populateDB, errorCB, successCB);	
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/viewnotes",
        data: {"user_id": userId},
        success: function (response) {
        	
        		//alert("1 "+JSON.stringify(response));
        		
        		db.transaction(function (tx) { saveDetailsInDB(tx, JSON.stringify(response)); } ,errorCB , successInsertion);

				//alert(response.length);
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function done(tx) {
	//alert("Positive FULLsuccess ");
}

function populateDB(tx) {
    tx.executeSql('DROP TABLE IF EXISTS Notes_'+userId);
    tx.executeSql('CREATE TABLE IF NOT EXISTS Notes_'+userId+' (ID INTEGER PRIMARY KEY,noteID TEXT,notebookId TEXT, notesJSON TEXT)');
}

function saveDetailsInDB(tx,response){
	//alert(response);
	
	window.plugins.spinnerDialog.hide();
	
	var JSONform = JSON.parse(response);
	var lengthOfArray = JSONform.length;
	
	for(var i=0; i<lengthOfArray; i++){
			var noteId = JSONform[i].id;
			var notesDetails = JSON.stringify(JSONform[i]);
			var notebookId = JSONform[i].notebook;
			
			//alert(notebookId);
			
			tx.executeSql('INSERT INTO Notes_'+userId+' (noteID,notebookId, notesJSON) VALUES (?,?,?)',[noteId,notebookId, notesDetails],done,errorCB);
	}
	storage.setItem("FirstNotesAddition","1");

	var a = storage.getItem("NotesToShow");
	if (a!= "-1") {
		tx.executeSql('SELECT * FROM Notes_'+userId+' WHERE notebookId='+a, [], querySuccess, errorCB);
	}else {
		window.location = "my-notes.html";
	}
}

// Transaction error callback
    
function errorCB(err) {
    //alert("Error processing SQL: "+err.code+" "+err.message);
    storage.setItem("FirstNotesAddition","0");
    alert("No notes available!\nKindly check your internet connection if there were notes but not shown here!");
}

// Transaction success callback

function successCB() {
  	 //alert("Positive successCB");
}

function successInsertion() {
  	 //alert("Positive successInsertion");
}

function querySuccess(tx,results){
		  var len = results.rows.length;
        //alert("Total data: "+len);
        var parentdiv = document.getElementById('divToAdding');
        
        while (parentdiv.hasChildNodes()) {
    			parentdiv.removeChild(parentdiv.lastChild);
			}
			storage.setItem("NotesToShow","-1");
        
        if(len >=1){
        	for (var i=0; i<len; i++){
        		/*alert(results.rows.item(i).ID);
        		alert(results.rows.item(i).noteID);
            alert(results.rows.item(i).notesJSON );*/
            
            var stringResponse = escape(results.rows.item(i).notesJSON);		
            var JSONValue = JSON.parse(results.rows.item(i).notesJSON);	
            
            //alert(JSON.stringify(JSONValue));		

				var newdiv = document.createElement('div');
				newdiv.innerHTML = '<div class="row upward" ><div class="col s12 m12"><div class="card white darken-1 overflow"><div class="card-content black-text"><span class="card-title">'+ setValue(escape(JSONValue.title)) +'</span><p>'+ setValue(escape(JSONValue.description)) +'</p></div><div class="card-action"><h6>Date : '+ JSONValue.created.split(" ")[0] +'</h6></div><div class="card-action"><a onclick="deleteNotes('+ JSONValue.id +')" >Delete</a><a class="modal-trigger" onclick="editNotes(\''+ stringResponse +'\');" >Edit</a></div></div></div></div>';

				
				
				parentdiv.appendChild(newdiv);
        	}  
        	
        	
     	} else {
     		alert("No notes available!");	
     	}

}

function makeDataWithDB(tx) {
		var a = storage.getItem("NotesToShow");
		//alert(a);
		if(a!= "-1"){
			tx.executeSql('SELECT * FROM Notes_'+userId+' WHERE notebookId='+a, [], querySuccess, errorCB);
		}else{
			window.location = "my-notes.html";		
		}
}



