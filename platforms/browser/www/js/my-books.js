var db = "";
var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

$(document).ready( function() {
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var storage = window.localStorage;	
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
	
	//var fullName = "dfdfdf";
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
	
	var userId = JSON.parse(storage.getItem("signInJSON")).id;  
	
	getAllBooks(userId);
	
	var itemsForSelection = storage.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}
	
});

function getAllBooks(userId) {
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/viewbooks",
        data: {"user_id": userId },
        success: function (response) {
        	
        		//alert(response.msg);
        		var totalCount = response.length;
        		//alert(totalCount);
        		
        		var parentdiv = document.getElementById('divToAdding');
        
        		while (parentdiv.hasChildNodes()) {
    				parentdiv.removeChild(parentdiv.lastChild);
				}
        		
        		if (totalCount>=1) {
        			for(var i=0; i<totalCount; i++){
        		
        				var newdiv = document.createElement('div');
        			
						newdiv.innerHTML = '<div class="row upward"><div class="col s12 m12"><a onclick=showbook('+response[i].id+') ><div class="card white darken-1 overflow"><div class="book-cover"></div><div class="card-content black-text"><span class="card-title">'+ setValue(escape(response[i].book_name)) +'</span><p>'+ setValue(escape(response[i].description)) +'</p></div></div></a></div></div>';        		
        		
        				parentdiv.appendChild(newdiv);
        			}
        		} else {
        			alert("No books available!");	
        		}	
            
        },
        error: function (errorMessage) {
        		//window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});   	

}

function showbook(id) {
	window.plugins.spinnerDialog.show(null,"Retrieving Chapters...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/showbook",
        data: {"book_id": id },
        success: function (response) {
        	
        	   var respLen = response.length;
        	   //alert(respLen);
        	   
        	   storage.setItem("respLen",respLen);
        	   storage.setItem("bookId",id);
        	   //storage.setItem("bookName",name_a);
        	  
        	   window.location = "internal-book.html";
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});	
	
	
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function setValue(result) {
	return unescape(result);	
}

function addNoteBook() {
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var emailId = receivedDataJSON.email;
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		
		//alert("1"+emailId+noteBookName);
		
		
		//alert(storage.getItem("NotebookNames"));
		
		
		window.plugins.spinnerDialog.show(null,"Adding Notebook...", true);
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotebook",
        data: {"email_id": emailId, "name":noteBookName},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		storage.setItem("FirstNotebookAddition","0");
        		refreshDBNoteBook(emailId);
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});		
	
	}
}

function refreshDBNoteBook(emailId) {
	
	//alert("1"+emailId);
	window.plugins.spinnerDialog.show(null,"Loading Data...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/listnotebook",
        data: {"email_id": emailId},
        success: function (response) {
        		var nbname = response.note_book_data;
        		
        		//alert(JSON.stringify(nbname));
        		window.plugins.spinnerDialog.hide();
        		
        		storage.setItem("NotebookNames","");
        		
        		for(var i=0; i<nbname.length; i++){
					var JSONValue = nbname[i].name; 
					storage.setItem("NotebookNames",storage.getItem("NotebookNames")+JSONValue+","); 
					//alert(storage.getItem("NotebookNames"));      		
        		
        		}
        		
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add a notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val();
	var notesOption = $('#notes_option').find(":selected").text();
	var notesValue = $("#textarea1").val();
	
	//alert(notesTitle+notesOption+notesValue);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		
		window.plugins.spinnerDialog.show(null,"Adding Notes...", true);
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		storage.setItem("FirstNotebookAddition","0");
				storage.setItem("FirstNotesAddition","0");
        		//refreshDB();
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}
