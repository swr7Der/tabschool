var storage = window.localStorage;

function onSuccess(imageData) {
    //alert(imageData);
    
    console.log(imageData);

    document.getElementById('pImage').src = "data:image/jpeg;base64,"+imageData;
    
    var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
    var emailId = receivedDataJSON.email;
    
    window.plugins.spinnerDialog.show(null,"Please wait...", true);
    
    $.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/updatephoto",
        data: {"email": emailId, "base_64":imageData, "extension":"jpeg"},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	
        		var a = JSON.stringify(response);
        		alert(a);
        		
        		
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
      
}

function onFail(message) {
    alert('Image uploading failed due to internal error! Try again after some time');
}

function changePic() {

	//alert("clicked");
	
	navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
    	destinationType: Camera.DestinationType.DATA_URL,
    	sourceType: Camera.PictureSourceType.PHOTOLIBRARY
		});
		
}