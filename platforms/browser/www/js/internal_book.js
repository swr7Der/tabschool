var db = "";
var storage = window.localStorage;

$(document).ready( function() {
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var storage = window.localStorage;
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
	
	var respLen = storage.getItem("respLen");
	var bookId = storage.getItem("bookId");
	
	changeOptions(1,bookId);
	
	//var a = storage.getItem("bookName");
	//$("#bookName").text(a);
	
	var select = document.getElementById("bookchapter");
	for(var i=0; i<respLen; i++){
					var opt = document.createElement('option');
					var k = i+1
               opt.value = i+1;
    				opt.innerHTML = "Chapter "+k;
    				select.appendChild(opt);
	}

	var itemsForSelection = storage.getItem("NotebookNames");
    var countOfSelection = itemsForSelection.split(",");

	for (var i=0; i<countOfSelection.length - 1; i++) { 
		var select = document.getElementById("notes_option");
		select.options[select.options.length] = new Option(countOfSelection[i], i+1);
		//alert(countOfSelection[i]);
	}  
});

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function changeOptions(val,id) {
	      $.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/showbook",
        data: {"book_id": id },
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	   
        	   
        		//alert(JSON.stringify(response[val]));
        		
        		var parentdiv = document.getElementById('showbook');
        		
        		while (parentdiv.hasChildNodes()) {
    				parentdiv.removeChild(parentdiv.lastChild);
				}
        		
        		var newdiv = document.createElement('div');
				newdiv.innerHTML = '<div class="card white darken-1 overflow"><div class="card-content black-text"><span class="card-title">Book Title</span><p>'+response[val-1].text+'</p></div></div>';
				parentdiv.appendChild(newdiv);
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});       
        	
}

$(document).on('change','#bookchapter',function(){
	var bookId = storage.getItem("bookId");
	
		var e = document.getElementById("bookchapter");
		var strUser = e.options[e.selectedIndex].value;
		//alert(strUser)
		window.plugins.spinnerDialog.show(null,"Retrieving Chapters...", true);
		changeOptions(strUser,bookId);
 });
 
 function addNoteBook() {
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var emailId = receivedDataJSON.email;
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		//alert("2222");
		
		//alert("1"+emailId+noteBookName);
		
		
		//alert(storage.getItem("NotebookNames"));
		window.plugins.spinnerDialog.show(null,"Adding Notebook...", true);
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotebook",
        data: {"email_id": emailId, "name":noteBookName},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		storage.setItem("FirstNotebookAddition","0");
				storage.setItem("FirstNotesAddition","0");
        		refreshDBNoteBook(emailId);
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});		
	
	}
}

function refreshDBNoteBook(emailId) {
	
	//alert("1"+emailId);
	window.plugins.spinnerDialog.show(null,"Getting Data...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/listnotebook",
        data: {"email_id": emailId},
        success: function (response) {
        		var nbname = response.note_book_data;
        		
        		//alert(JSON.stringify(nbname));
        		window.plugins.spinnerDialog.hide();
        		
        		storage.setItem("NotebookNames","");
        		
        		for(var i=0; i<nbname.length; i++){
					var JSONValue = nbname[i].name; 
					storage.setItem("NotebookNames",storage.getItem("NotebookNames")+JSONValue+","); 
					//alert(storage.getItem("NotebookNames"));      		
        		
        		}
        		
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add a notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val();
	var notesOption = $('#notes_option').find(":selected").text();
	var notesValue = $("#textarea1").val();
	
	//alert(notesTitle+notesOption+notesValue);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Adding Notes...", true);
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		storage.setItem("FirstNotebookAddition","0");
				storage.setItem("FirstNotesAddition","0");
        		//refreshDB();
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}

