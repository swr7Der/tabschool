var db = "";
var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

$(document).ready( function() {
	document.addEventListener("backbutton", leavePage, false); 
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var savedData  = window.localStorage;
	
	//alert("11111");
	
	var receivedDataJSON = JSON.parse(savedData.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	//window.alert(fullName);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
    
    var emailId = receivedDataJSON.email;
    //alert(emailId);
    
    getAllClassfeed(emailId);
    
    var userId = JSON.parse(storage.getItem("signInJSON")).id;
    //alert(userId);
    
    var itemsForSelection = savedData.getItem("NotebookNames");
    var countOfSelection = itemsForSelection.split(",");

	for (var i=0; i<countOfSelection.length - 1; i++) { 
		var select = document.getElementById("notes_option");
		select.options[select.options.length] = new Option(countOfSelection[i], i+1);
		//alert(countOfSelection[i]);
	}   
    
});

function getAllClassfeed(emailId) {

	$.ajax({
			type: 'POST',
			url: "http://vnenterprizes.in/tabschool/index.php/api/classfeedview",
			data: {"email_id": emailId},
			success: function (response) {
					//alert(JSON.stringify(response));
					var count = response.length;
					
					var parentdiv = document.getElementById('divToAdding');
        
        			while (parentdiv.hasChildNodes()) {
    					parentdiv.removeChild(parentdiv.lastChild);
					}
        		
        			if (count>=1) {
        				for(var i=0; i<count; i++){
        		
        					var newdiv = document.createElement('div');
        			
							newdiv.innerHTML = '<div class="row upward"><div class="col s12 m12"><div class="card white darken-1"><div class="card-content black-text"><span class="card-title">'+ setValue(escape(response[i].subject)) +'</span><p>'+setValue(escape(response[i].content))+'</p></div></div></div></div>';        		
        		
        					parentdiv.appendChild(newdiv);
        				}
        			} else {
        				alert("No classfeed available!");		
        			}						
			},
			error: function (errorMessage) {
				alert("Something went wrong. Please restart the app with active internet connection!");
			}
		
	});	
	
}

function setValue(result) {
	return unescape(result);	
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function leavePage() {
	var count = 0;
	navigator.notification.confirm("Are you sure you want to exit app?",onConfirm,"Exit app?",["Yes","No"]);
}

function onConfirm(button) {
	if (button == 1) {
		navigator.app.exitApp();	
	}
}

function addNoteBook() {
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var emailId = receivedDataJSON.email;
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		
		
		//alert(storage.getItem("NotebookNames"));
		
		window.plugins.spinnerDialog.show(null,"Adding Notebook...", true);
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotebook",
        data: {"email_id": emailId, "name":noteBookName},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		 storage.setItem("FirstNotebookAddition","0");
        		refreshDBNoteBook(emailId);
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});		
	
	}
}

function refreshDBNoteBook(emailId) {
	
	//alert("1"+emailId);
	window.plugins.spinnerDialog.show(null,"Loading Data...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/listnotebook",
        data: {"email_id": emailId},
        success: function (response) {
        		var nbname = response.note_book_data;
        		window.plugins.spinnerDialog.hide();
        		
        		//alert(JSON.stringify(nbname));
        		
        		storage.setItem("NotebookNames","");
        		
        		for(var i=0; i<nbname.length; i++){
					var JSONValue = nbname[i].name; 
					storage.setItem("NotebookNames",storage.getItem("NotebookNames")+JSONValue+","); 
					//alert(storage.getItem("NotebookNames"));      		
        		
        		}
        		
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add a notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val();
	var notesOption = $('#notes_option').find(":selected").text();
	var notesValue = $("#textarea1").val();
	
	//alert(notesTitle+notesOption+notesValue);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Adding Notes...", true);
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		storage.setItem("FirstNotebookAddition","0");
				storage.setItem("FirstNotesAddition","0");
        		//refreshDB();
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}
