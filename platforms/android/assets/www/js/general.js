$(document).ready( function() {

	var savedData  = window.localStorage;
	
	var receivedDataJSON = JSON.parse(savedData.getItem('signInJSON'));
	
	var firstName = titleCase(receivedDataJSON.first_name);
	var lastName = titleCase(receivedDataJSON.last_name);
   
   $("#first_name").val(firstName);
   $("#last_name").val(lastName);
});

function saveNewPW() {
	
	var savedData  = window.localStorage;
	var userId = (JSON.parse(savedData.getItem('signInJSON'))).id;
	
	var newPW = $("#newpassword").val();
	var retypeNewPW = $("#retypeNewpassword").val();
	var newFirstName = $("#first_name").val().trim();
	var newLastName = $("#last_name").val().trim();
	
	if (newPW.length === 0 || retypeNewPW.length === 0) {
		window.alert("Password field cannot be empty!");
	}
	
	else {
		
		if (newPW !== retypeNewPW) {
			alert("Password doesn't match !")
		}
		else {
			
			//alert(userId+" "+newPW);
			window.plugins.spinnerDialog.show(null,"Updating Password...", true);
			
			$.ajax({
				
				type: 'POST',
				url: 'http://vnenterprizes.in/tabschool/index.php/api/updatepassword',
				data: { "user_id": userId, "new_password":newPW },
				
				success: function (response) {
					window.plugins.spinnerDialog.hide();
					alert(response.msg);
					window.location = "settings.html";	
				},
				
				error: function (errorMessage) {
					window.plugins.spinnerDialog.hide();
					alert("Something went wrong. Please restart the app with active internet connection!");	
				}
					
			});
			
		}
	}
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}
