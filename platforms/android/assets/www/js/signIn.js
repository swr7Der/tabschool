var db = "";

$(document).ready( function() {
	
	var storage = window.localStorage;
	if(storage.getItem("signInDetails") != null){
			window.location = "classfeed.html";
	}

});

function signInStarting() {
	
	var email = $("#email").val().trim();
	var password = $("#password").val();
	
	var storage = window.localStorage;
	
	storage.setItem("NotebookNames","");
	storage.setItem("FirstNotebookAddition","0");
	storage.setItem("FirstNotesAddition","0");

	if (email.length === 0 || password.length === 0) {
		window.alert("All fields are necessary. Fields cannot be empty!");
	}
	else {
   	
   	if(!validateEmail(email)){
			alert("Please enter a valid email address");   	
   	} else {
   		
   		window.plugins.spinnerDialog.show(null,"Logging in...", true);
   		
		
			$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/login",
        data: {"name": email, "pass": password},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	
        		if(JSON.stringify(response[0]) == null){
					alert("Either email or password is wrong!")        		
        		} else {
        			
        			storage.setItem("signInJSON",JSON.stringify(response[0]));
        			
        			storage.setItem("signInDetails",email+"&"+password);
        		
					var json = JSON.stringify(response[0]);
				
					//alert(json);  
					
					window.location = "classfeed.html";
        				
        		}
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
 		
 	}		
		
	}	
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
  
 }
 
 
 
 
 
