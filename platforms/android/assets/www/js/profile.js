var storage = window.localStorage;

$(document).ready(function () {
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
	
   document.getElementById("pImage").src = link;
})


function onSuccess(imageData) {
    //alert(imageData);
    
    console.log(imageData);
    
    var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
    var emailId = receivedDataJSON.email;
    
    window.plugins.spinnerDialog.show(null,"Please wait...", true);
    
    $.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/updatephoto",
        data: {"email": emailId, "base_64":imageData, "extension":"jpeg"},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        		
        		signInForPicUpdate();
        		
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
      
}

function onFail(message) {
    alert('Image uploading failed due to internal error! Try again after some time');
}

function changePic() {

	//alert("clicked");
	
	navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
    	destinationType: Camera.DestinationType.DATA_URL,
    	sourceType: Camera.PictureSourceType.PHOTOLIBRARY
		});
		
}

function logout() {
	var storage = window.localStorage;
	storage.removeItem("signInDetails");
	storage.removeItem("signInJSON");
	window.location = "index.html";	
}

function signInForPicUpdate() {
	
	var signInDetails = storage.getItem("signInDetails");
	
	var details = signInDetails.split("&");
	
	var email = details[0];
	var password = details[1];
	
   		
   window.plugins.spinnerDialog.show(null,"Please wait...", true);
   		
		
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/login",
        data: {"name": email, "pass": password},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        
        		storage.setItem("signInJSON",JSON.stringify(response[0]));
				var json = JSON.stringify(response[0]);
				
				//alert(json);  
					
				window.location = "classfeed.html";
        				
        	},
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    });
 	
}