var db = "";
var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

$(document).ready( function() {
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var storage = window.localStorage;	
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
	
	var emailId = receivedDataJSON.email;  
	
	getAllTasks(emailId);
	
	var itemsForSelection = storage.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}
		
		
	
});

function getAllTasks(emailId) {
	
	$.ajax({
			
        type: 'POST',
        url: " http://vnenterprizes.in/tabschool/index.php/api/taskmanagerview",
        data: {"email_id": emailId },
        success: function (response) {
        	
        		//alert(JSON.stringify(response));
        		var totalCount = response.length;
        		//alert(totalCount);
        		
        		var parentdiv = document.getElementById('divToAdding');
        
        		while (parentdiv.hasChildNodes()) {
    				parentdiv.removeChild(parentdiv.lastChild);
				}
        		
        		if (totalCount>=1) {
        			for(var i=0; i<totalCount; i++){
        		
        				var newdiv = document.createElement('div');
        			
						newdiv.innerHTML = '<div class="row upward"><div class="col s12 m12"><div class="card white darken-1 overflow"><div class="card-content black-text"><p>'+setValue(escape(response[i].name))+'</p></div><div class="card-action"><a onclick="deleteTask('+response[i].id+')">Delete</a><a onclick="doneTask('+response[i].id+')" class="modal-trigger">Done</a></div></div>';        		
        		
        				parentdiv.appendChild(newdiv);
        			}
        		} else {
        			//alert("No books available!");	
        		}	
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});   	

}

function createTask() {
	
		var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
		var emailId = receivedDataJSON.email;
	
		var taskTitle = $("#add_task_title").val(); 
		var dateAddTask = $("#date_add_task").val();
		var ampmAddTask = $('#ampm_add_task').find(":selected").text();
		var hourAddTask = $("#hour_add_task").find(":selected").text();
		var minAddTask = $("#minutes_add_task").find(":selected").text();
		
		//alert(taskTitle+dateAddTask+ampmAddTask+hourAddTask+minAddTask);
		
		if (taskTitle.length == 0 || dateAddTask.length == 0) {
			alert("All fields are necessary!");	
		}else if (parseInt(hourAddTask,10) > 12 || parseInt(hourAddTask,10) < 0 || hourAddTask == "Hours") {
			alert("Hour must be between 00 and 12");	
		} else if (parseInt(minAddTask,10) > 59 || parseInt(minAddTask,10) < 0 || minAddTask == "Minutes") {
			alert("Minutes must be between 00 and 59");	
		} /*else if (taskTitle.length == 0 || dateAddTask.length == 0 || hourAddTask.length == 0 || minAddTask.length == 0) {
			alert("All fields are necessary!");	
		} else if (hourAddTask.length !=2 || minAddTask.length !=2) {
			alert("Please enter hour and minutes to set reminder!");	
		}*/ else {
			var taskDetails = taskTitle + " at " + hourAddTask + ":" + minAddTask + " "+ampmAddTask;
			
			var time = "";
			if(parseInt(hourAddTask,10) == 12 ){
				if (ampmAddTask == 'AM') {
					time = '00:'+ minAddTask + ":00";	
				} else {
					time = '12:'+ minAddTask + ":00";	
				}			
			} else {
				if (ampmAddTask == 'AM') {
					time = hourAddTask + ':'+ minAddTask + ":00";
				} else {
					time = (parseInt(hourAddTask,10)+12)+':'+ minAddTask + ":00";	
				}
				
			}
			
			var schedule_time = new Date((dateAddTask + " "+time ).replace(/-/g, "/")).getTime();
	      schedule_time = new Date(schedule_time);
	      
	      //alert(schedule_time);
		
			window.plugins.spinnerDialog.show(null,"Creating Task...", true);
			$.ajax({
			
        		type: 'POST',
        		url: "http://vnenterprizes.in/tabschool/index.php/api/addtaskmanager",
        		data: {"email_id": emailId, "title": taskDetails, "date":dateAddTask},
        		success: function (response) {
        			window.plugins.spinnerDialog.hide();
        	
        			alert(response.msg);
        			
        			cordova.plugins.notification.local.hasPermission(function(granted){
						
          	  		if(granted == true)
          	  		{
          	    		schedule(response.id, "Reminder", taskTitle, schedule_time);
          	 		 }
          	  		else
          	  		{
          	   		cordova.plugins.notification.local.registerPermission(function(granted) {
          	        	if(granted == true)
          	        	{
          	          	schedule(response.id, "Reminder", taskTitle, schedule_time);
          	        }
          	        else
          	        {
          	          navigator.notification.alert("Reminder cannot be added because app doesn't have permission");
          	        }
          	    });
          	  }
          	}); 			
        			
        			window.location = "task-manager.html";
            
        		},
        		error: function (errorMessage) {
        			window.plugins.spinnerDialog.hide();
        			alert("Something went wrong. Please restart the app with active internet connection!");
        		
        		}
        
    		});
			
			
			
		}	
}

function doneTask(id) {
	//$("#edit").show();
	alert("Congratulations!! You have completed your Task");
	
	window.plugins.spinnerDialog.show(null,"Removing Task...", true);
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/deletetaskmanager",
        data: {"task_id":id},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert("Task removed from your task manager");
        		cancelSchedule(id);
        		window.location = "task-manager.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
}

function deleteTask(id) {	
	//alert(id);
	window.plugins.spinnerDialog.show(null,"Deleting Task...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/deletetaskmanager",
        data: {"task_id":id},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		cancelSchedule(id);
        		window.location = "task-manager.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function setValue(result) {
	return unescape(result);	
}

function addNoteBook() {
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var emailId = receivedDataJSON.email;
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		
		//alert("1"+emailId+noteBookName);
		
		
		//alert(storage.getItem("NotebookNames"));
		window.plugins.spinnerDialog.show(null,"Adding Notebook...", true);
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotebook",
        data: {"email_id": emailId, "name":noteBookName},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		 storage.setItem("FirstNotebookAddition","0");
        		refreshDBNoteBook(emailId);
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});		
	
	}
}

function refreshDBNoteBook(emailId) {
	
	//alert("1"+emailId);
	window.plugins.spinnerDialog.show(null,"Getting Data...", true);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/listnotebook",
        data: {"email_id": emailId},
        success: function (response) {
        		var nbname = response.note_book_data;
        		window.plugins.spinnerDialog.hide();
        		
        		//alert(JSON.stringify(nbname));
        		
        		storage.setItem("NotebookNames","");
        		
        		for(var i=0; i<nbname.length; i++){
					var JSONValue = nbname[i].name; 
					storage.setItem("NotebookNames",storage.getItem("NotebookNames")+JSONValue+","); 
					//alert(storage.getItem("NotebookNames"));      		
        		
        		}
        		
        		window.location = "my-notes.html";
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add a notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val();
	var notesOption = $('#notes_option').find(":selected").text();
	var notesValue = $("#textarea1").val();
	
	//alert(notesTitle+notesOption+notesValue);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Adding Notes...", true);
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
				storage.setItem("FirstNotesAddition","0");
        		//refreshDB();
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}



function schedule(id, title, message, schedule_time)
{
          	cordova.plugins.notification.local.schedule({
          	    id: id,
          	    title: title,
          	    message: message,
          	    firstAt: schedule_time,
          	    sound: 'file://res/raw/notification_sound.mp3',
    				 icon: "file://icon.png",
    				 smallIcon: "res://notification_icon"
          	});
         
}

function cancelSchedule(id) {
		cordova.plugins.notification.local.cancel(id, function () { console.log("Reminder deleted!") });

}