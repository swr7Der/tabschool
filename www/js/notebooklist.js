var db = "";
var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

$(document).ready( function() {
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var savedData  = window.localStorage;
	
	var receivedDataJSON = JSON.parse(savedData.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
    
    var emailId = receivedDataJSON.email;
    
    if (storage.getItem("FirstNotebookAddition") == '1') {
			db.transaction(makeDataWithDB, errorCB, successCB);
	 }
	
	 if (storage.getItem("FirstNotebookAddition") == '0') {
	 		//("storrage1");
			refreshDB(emailId);
			
	 }
    
    var userId = JSON.parse(storage.getItem("signInJSON")).id;
    //alert(userId);
    
    var itemsForSelection = savedData.getItem("NotebookNames");
    var countOfSelection = itemsForSelection.split(",");

	for (var i=0; i<countOfSelection.length - 1; i++) { 
		var select = document.getElementById("notes_option");
		select.options[select.options.length] = new Option(countOfSelection[i], i+1);
		//alert(countOfSelection[i]);
	}   
    
});

function setValue(result) {
	return unescape(result);	
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function addNoteBook() {
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	var emailId = receivedDataJSON.email;
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Adding Notebook...", true);
		//alert("1"+emailId+noteBookName);
		
		
		//alert(savedData.getItem("NotebookNames"));
		//window.location = "classfeed.html";*/
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotebook",
        data: {"email_id": emailId, "name":noteBookName},
        success: function (response) {
        	
        		window.plugins.spinnerDialog.hide();
        	
        		alert(response.msg);
        		
        		refreshDB(emailId);
        },
        
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});		
		
		
		
	}
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add at least one notebook for adding notes!");   
   }	
	
	var notesTitle = $("#note_title").val().trim();
	var notesOption = $('#notes_option').find(":selected").text();
	var notesValue = $("#textarea1").val().trim();
	
	//alert(notesTitle+notesOption+notesValue+userId);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		window.plugins.spinnerDialog.show(null,"Adding Notes...", true);
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        		window.plugins.spinnerDialog.hide();
        		//alert(JSON.stringify(response));
        		alert(response.msg);
	         storage.setItem("FirstNotesAddition","0");

        		//refreshDB();
            
        },
        error: function (errorMessage) {
        		window.plugins.spinnerDialog.hide();
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}


function refreshDB(emailId) {
	
	//alert("1"+emailId);

	db.transaction(populateDB, errorCB, successCB);	
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/listnotebook",
        data: {"email_id": emailId},
        success: function (response) {
        	
        		//alert("1 "+JSON.stringify(response));
        		
        		db.transaction(function (tx) { saveDetailsInDB(tx, JSON.stringify(response)); } ,errorCB , successInsertion);

				//alert(response.length);
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}


function done(tx) {
	//alert("Positive FULLsuccess ");
}

function populateDB(tx) {
    tx.executeSql('DROP TABLE IF EXISTS Notebooks_'+userId);
    tx.executeSql('CREATE TABLE IF NOT EXISTS Notebooks_'+userId+' (ID INTEGER PRIMARY KEY,notebookId TEXT, notesJSON TEXT)');
}

function saveDetailsInDB(tx,response){
	//alert(response);
	
	var valueinJSON = JSON.parse(response);
	var JSONform = valueinJSON.note_book_data;
	//alert(JSONform);
	var lengthOfArray = JSONform.length;
	//alert(lengthOfArray);
	
	for(var i=0; i<lengthOfArray; i++){
			var notebookId = JSONform[i].id;
			var notesbookDetails = JSONform[i].name;
			
			//alert(notesbookDetails);
			
			tx.executeSql('INSERT INTO Notebooks_'+userId+' (notebookId, notesJSON) VALUES (?,?)',[notebookId, notesbookDetails],done,errorCB);
	}
	
	var JSONform2 = valueinJSON.mybook;
	//alert(JSONform2);
	var lengthOfArray2 = JSONform2.length;
	
	for(var i=0; i<lengthOfArray2; i++){
			var notebookId1 = JSONform2[i].id;
			var notesbookDetails1 = JSONform2[i].book_name;
			
			//alert(notesbookDetails1);
			
			tx.executeSql('INSERT INTO Notebooks_'+userId+' (notebookId, notesJSON) VALUES (?,?)',[notebookId1, notesbookDetails1],done,errorCB);
	}
	
	tx.executeSql('SELECT * FROM Notebooks_'+userId, [], querySuccess, errorCBa);

}

// Transaction error callback
    
function errorCB(err) {
    //alert("Error processing SQL: "+err.code+" "+err.message);
    alert("Some error has occurred in saving notes!");
}

function errorCBa(err) {
    //alert("Error processi: "+err.code+" "+err.message);
    alert("Some error has occurred in retrieving notes!");
}

// Transaction success callback

function successCB() {
  	 //alert("Positive successCB");
}

function makeDataWithDB(tx) {
		tx.executeSql('SELECT * FROM Notebooks_'+userId, [], querySuccess, errorCBa);
}

function querySuccess(tx,results){
		  var len = results.rows.length;
        //alert("Total data: "+len);
        var parentdiv = document.getElementById('divToAdding');
        
        //var JSONValue = JSON.parse(results.rows.item(0).notesJSON);	
        //alert(JSON.stringify(JSONValue));
        
         storage.setItem("NotebookNames","");
        
        storage.setItem("FirstNotebookAddition","1");
        
        while (parentdiv.hasChildNodes()) {
    			parentdiv.removeChild(parentdiv.lastChild);
			}
        
        if(len >=1){
        	for (var i=0; i<len; i++){
        		/*alert(results.rows.item(i).ID);
        		alert(results.rows.item(i).notebookId);
            alert(results.rows.item(i).notesJSON );
            
            
            var stringResponse = escape(results.rows.item(i).notesJSON);		
            var JSONValue = JSON.parse(results.rows.item(i).notesJSON);		*/
            
            var id = results.rows.item(i).notebookId;
            var name = results.rows.item(i).notesJSON;
            
            storage.setItem("NotebookNames",storage.getItem("NotebookNames")+ name+",");
            //alert(storage.getItem("NotebookNames"));	
            
				var newdiv = document.createElement('div');
				newdiv.innerHTML = '<div class="row upward"><div class="col s12 m12"><a onclick="getnotes('+ id +')"><div class="card white darken-1 overflow"><div class="book-cover"></div><div class="card-content black-text"><span class="card-title">'+name+'</span><p></p></div></div></a></div></div>';
				parentdiv.appendChild(newdiv);
        	}   
        	
        	var itemsForSelection = storage.getItem("NotebookNames");
    		var countOfSelection = itemsForSelection.split(",");

			for (var i=0; i<countOfSelection.length - 1; i++) { 
				var select = document.getElementById("notes_option");
				select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			} 
     	} else {
     		alert("No notes available!");	
     	}
}

function getnotes(id) {
		//alert(id);
		storage.setItem("NotesToShow",id);
		if (storage.getItem("FirstNotesAddition") != "1") {
			window.plugins.spinnerDialog.show(null,"Fetching Notes...", true);
		}
		window.location = "book-notes.html";	
}

function successInsertion() {
  	 //alert("Positive successInsertion");
}
