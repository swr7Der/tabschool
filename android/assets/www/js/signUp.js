var firstName;
var lastName;
var email;
var password;
var retypePassword;
var gender;
var DOB;
var institute;
var course;
var branch;
var semester;
var teachers = [];

var storage = window.localStorage;

var details = [];

function signUpStarting() {
	
	wordsCount = storage.getItem("signUpValue").split(',');
    
     for(var i = 0; i < wordsCount.length; i++) {
          details[i] = wordsCount[i];
     }
	
	$.ajax({
			
        type: 'POST',
        url: " http://vnenterprizes.in/tabschool/index.php/api/register",
        data: {"first_name": details[0], "last_name": details[1], "email":details[2],"password": details[3] ,"sex":details[4],"birthday": details[5]+","+details[6],"college":details[7] , "teacher_id":"","semesters":details[10]},
        			
        success: function (response) {
        		alert(response.msg);
        		if (response.msg === "Registration successfully done .") {
        			storage.removeItem("signUpValue");
        			window.location = "index.html";
        		}
				
            
        },
        error: function (errorMessage) {
        		alert(errorMessage.msg);
        	
        }
        
    		});
		
}	

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function signUp1() {
	firstName = $("#first_name").val().trim();
	lastName = $("#last_name").val().trim();
	email = $("#email").val().trim();
	password = $("#password").val();
	retypePassword = $("#retype_password").val();	
	
	
	if((firstName.length == 0 && lastName.length == 0) || email.length == 0 || password.length == 0 || retypePassword.length == 0){
			alert("All fields are necessary. Fields cannot be empty!");
	} else {
			if(!validateEmail(email)){
				alert("Please enter a valid email");			
			} else if (password !== retypePassword) {
				alert("Password doesn't match!");	
			} else {
				details.push(firstName);
				details.push(lastName);
				details.push(email);
				details.push(password);				
				
				storage.setItem("signUpValue",details);
				window.location = "register-two.html";
				//alert(details);	
			}
	}
		
}

function signUp2() {
	gender = $('#gender').find(":selected").text();
	DOB = $('#DOB').val();
	
	//var select = document.getElementById("gender");
	//select.options[select.options.length] = new Option('Text 1', 'Value1');
	
	if (gender.length===0 || $('#gender').find(":selected").attr("value")==="" || DOB.length===0) {
		window.alert("All fields are necessary. Fields cannot be empty!");
	} else {
		details.push(gender);
		details.push(DOB);
		storage.setItem("signUpValue",storage.getItem("signUpValue")+","+details);
		//alert(storage.getItem("signUpValue"));
		window.location = "register-three.html";
	}
}

function signUp3() {
	institute = $('#institute').find(":selected").attr('value');
	course = $('#course').find(":selected").text();
	branch = $('#branch').find(":selected").text();
	semester = $('#semester').find(":selected").attr('value');
	//alert(institute+" "+course+" "+branch+" "+semester);
	
	if ($('#institute').find(":selected").attr('value')==="" || 
		 $('#course').find(":selected").attr('value')==="" ||
		 $('#branch').find(":selected").attr('value')==="" ||
		 $('#semester').find(":selected").attr('value')==="") {
		 window.alert("All fields are necessary. Fields cannot be empty!");
	} else {
		details.push(institute);
		details.push(course);
		details.push(branch);
		details.push(semester);
		
		storage.setItem("signUpValue",storage.getItem("signUpValue")+","+details);
		//alert(storage.getItem("signUpValue"));
		window.location = "register-four.html";
	}
}

function signUp4() {
	$(":checkbox" ).map(function() {
			if($(this).is(':checked')){
					teachers.push($('label[for="' + this.id + '"]').text());
			}
	});
	if (teachers.length===0) {
		window.alert("Please select at least one teacher!");
	} else {
		details.push(teachers);
		storage.setItem("signUpValue",storage.getItem("signUpValue")+","+details);
		//alert(storage.getItem("signUpValue"));
	}	
	
	signUpStarting();
}