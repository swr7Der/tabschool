var db = "";
var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

$(document).ready( function() {
	document.addEventListener("backbutton", leavePage, false); 
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var savedData  = window.localStorage;
	
	var receivedDataJSON = JSON.parse(savedData.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	//window.alert(fullName);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
    
    var emailId = receivedDataJSON.email;
    //alert(emailId);
    
    getAllClassfeed(emailId);
    
    var userId = JSON.parse(storage.getItem("signInJSON")).id;
    //alert(userId);
    
    var itemsForSelection = savedData.getItem("NotebookNames");
    var countOfSelection = itemsForSelection.split(",");

	for (var i=0; i<countOfSelection.length - 1; i++) { 
		var select = document.getElementById("notes_option");
		select.options[select.options.length] = new Option(countOfSelection[i], i+1);
		//alert(countOfSelection[i]);
	}   
    
});

function getAllClassfeed(emailId) {

	$.ajax({
			type: 'POST',
			url: "http://vnenterprizes.in/tabschool/index.php/api/classfeedview",
			data: {"email_id": emailId},
			success: function (response) {
					//alert(JSON.stringify(response));
					var count = response.length;
					
					var parentdiv = document.getElementById('divToAdding');
        
        			while (parentdiv.hasChildNodes()) {
    					parentdiv.removeChild(parentdiv.lastChild);
					}
        		
        			if (count>=1) {
        				for(var i=0; i<count; i++){
        		
        					var newdiv = document.createElement('div');
        			
							newdiv.innerHTML = '<div class="row upward"><div class="col s12 m12"><div class="card white darken-1"><div class="card-content black-text"><span class="card-title">'+ setValue(escape(response[i].subject)) +'</span><p>'+setValue(escape(response[i].content))+'</p></div></div></div></div>';        		
        		
        					parentdiv.appendChild(newdiv);
        				}
        			} else {
        				alert("No classfeed available!");		
        			}						
			},
			error: function (errorMessage) {
				alert("Something went wrong. Please restart the app with active internet connection!");
			}
		
	});	
	
}

function setValue(result) {
	return unescape(result);	
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function leavePage() {
	var count = 0;
	navigator.notification.confirm("Are you sure you want to exit app?",onConfirm,"Exit app?",["Yes","No"]);
}

function onConfirm(button) {
	if (button == 1) {
		navigator.app.exitApp();	
	}
}

function addNoteBook() {
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		
		var savedData  = window.localStorage;
		savedData.setItem("NotebookNames",savedData.getItem("NotebookNames")+ noteBookName+",");
		//alert(savedData.getItem("NotebookNames"));
		window.location = "classfeed.html";
		
	}
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add at least one notebook for adding notes!");   
   }	
	
	var notesTitle = $("#note_title").val().trim();
	var notesOption = $('#notes_option').find(":selected").attr('value');
	var notesValue = $("#textarea1").val().trim();
	
	//alert(notesTitle+notesOption+notesValue+userId);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        	
        		alert(response.msg);
        		refreshDB();
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}


function refreshDB() {

	db.transaction(populateDB, errorCB, successCB);	
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/viewnotes",
        data: {"user_id": userId},
        success: function (response) {
        	
        		//alert("1 "+JSON.stringify(response));
        		
        		db.transaction(function (tx) { saveDetailsInDB(tx, JSON.stringify(response)); } ,errorCB , successInsertion);

				//alert(response.length);
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}


function done(tx) {
	//alert("Positive FULLsuccess ");
}

function populateDB(tx) {
    tx.executeSql('DROP TABLE IF EXISTS Notes_'+userId);
    tx.executeSql('CREATE TABLE IF NOT EXISTS Notes_'+userId+' (ID INTEGER PRIMARY KEY,noteID TEXT, notesJSON TEXT)');
}

function saveDetailsInDB(tx,response){
	//alert(response);
	
	var JSONform = JSON.parse(response);
	var lengthOfArray = JSONform.length;
	
	for(var i=0; i<lengthOfArray; i++){
			var noteId = JSONform[i].id;
			var notesDetails = JSON.stringify(JSONform[i]);
			
			tx.executeSql('INSERT INTO Notes_'+userId+' (noteID, notesJSON) VALUES (?,?)',[noteId, notesDetails],done,errorCB);
	}

}

// Transaction error callback
    
function errorCB(err) {
    //alert("Error processing SQL: "+err.code+" "+err.message);
    alert("Some error has occurred in saving notes!");
}

// Transaction success callback

function successCB() {
  	 //alert("Positive successCB");
}

function successInsertion() {
  	 //alert("Positive successInsertion");
}
