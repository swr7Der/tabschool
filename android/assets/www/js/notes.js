var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

var db = "";

$(document).ready( function() {
	
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	db.transaction(makeDataWithDB, errorCB, successCB);	
  		
	var storage = window.localStorage;
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
	
	var userId = JSON.parse(storage.getItem("signInJSON")).id;
	
	//alert(userId);
	
	var itemsForSelection = storage.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}
	
});

function addNoteBook() {
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		
		var savedData  = window.localStorage;
		savedData.setItem("NotebookNames",savedData.getItem("NotebookNames")+ noteBookName+",");
		//alert(savedData.getItem("NotebookNames"));
		var itemsForSelection = savedData.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}   
		
	}
}

function addNotes() {
	
	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add at least one notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val();
	var notesOption = $('#notes_option').find(":selected").attr('value');
	var notesValue = $("#textarea1").val();
	
	//alert(notesTitle+notesOption+notesValue);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        	
        		alert(response.msg);
        		refreshDB();
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}

function editNotes(descriptionValue) {
	
	var a = 	unescape(descriptionValue);
	
	//alert(a);
	
	var descriptionUnescaped = JSON.parse(a);
	
	var noteId = descriptionUnescaped.id;
	var descriptionToSet = descriptionUnescaped.description;
	
	//alert(noteId+" "+descriptionToSet);
	
	$("#edit").show();
	
	$("#textarea2").val(descriptionToSet);
	
	document.getElementById("cancel_edit_save").onclick = function (e) { 
		 $("#edit").hide();
	}
	
	
	document.getElementById("edit_save").onclick = function (e) {
		
		$("#edit").hide(); 
	
		var descriptionToSend = $("#textarea2").val();
		
		//alert(noteId+" "+descriptionToSend);		
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/updatenotes",
        data: {"noteid": noteId, "description":descriptionToSend},
        success: function (response) {
        	
        		alert(response.msg);
        		refreshDB();
        		
        		//window.location = "book-notes.html";
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	}); 
	}
	
	
}

function deleteNotes(id) {
	
	//alert(id);
	$("#delete").show();	
	
	document.getElementById("dont_delete").onclick = function (e) { 
		$("#delete").hide(); 
	}	
	
	document.getElementById("delete_now").onclick = function (e) {
		$("#delete").hide();
			
		$.ajax({
			
			type: 'POST',
			url: 'http://vnenterprizes.in/tabschool/index.php/api/deletenotes',
			data: { "noteid":id },
			
			success: function (response) {
				alert(response.msg);
				refreshDB();	
				//window.location = "book-notes.html";
			},
			error: function (errorMessage) {
				alert(errorMessage);	
			}
			
		});
	}
	
}

function setValue(result) {
	return unescape(result);	
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}


function refreshDB() {

	db.transaction(populateDB, errorCB, successCB);	
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/viewnotes",
        data: {"user_id": userId},
        success: function (response) {
        	
        		//alert("1 "+JSON.stringify(response));
        		
        		db.transaction(function (tx) { saveDetailsInDB(tx, JSON.stringify(response)); } ,errorCB , successInsertion);

				//alert(response.length);
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}

function done(tx) {
	//alert("Positive FULLsuccess ");
}

function populateDB(tx) {
    tx.executeSql('DROP TABLE IF EXISTS Notes_'+userId);
    tx.executeSql('CREATE TABLE IF NOT EXISTS Notes_'+userId+' (ID INTEGER PRIMARY KEY,noteID TEXT, notesJSON TEXT)');
}

function saveDetailsInDB(tx,response){
	//alert(response);
	
	var JSONform = JSON.parse(response);
	var lengthOfArray = JSONform.length;
	
	for(var i=0; i<lengthOfArray; i++){
			var noteId = JSONform[i].id;
			var notesDetails = JSON.stringify(JSONform[i]);
			
			tx.executeSql('INSERT INTO Notes_'+userId+' (noteID, notesJSON) VALUES (?,?)',[noteId, notesDetails],done,errorCB);
	}

	tx.executeSql('SELECT * FROM Notes_'+userId, [], querySuccess, errorCB);
}

// Transaction error callback
    
function errorCB(err) {
    //alert("Error processing SQL: "+err.code+" "+err.message);
    alert("Some error has occurred in saving notes!");
}

// Transaction success callback

function successCB() {
  	 //alert("Positive successCB");
}

function successInsertion() {
  	 //alert("Positive successInsertion");
}

function querySuccess(tx,results){
		  var len = results.rows.length;
        //alert("Total data: "+len);
        var parentdiv = document.getElementById('divToAdding');
        
        while (parentdiv.hasChildNodes()) {
    			parentdiv.removeChild(parentdiv.lastChild);
			}
        
        if(len >=1){
        	for (var i=0; i<len; i++){
        		/*alert(results.rows.item(i).ID);
        		alert(results.rows.item(i).noteID);
            alert(results.rows.item(i).notesJSON );*/
            
            var stringResponse = escape(results.rows.item(i).notesJSON);		
            var JSONValue = JSON.parse(results.rows.item(i).notesJSON);			

				var newdiv = document.createElement('div');
				newdiv.innerHTML = '<div class="row upward" ><div class="col s12 m12"><div class="card white darken-1 overflow"><div class="card-content black-text"><span class="card-title">'+ setValue(escape(JSONValue.title)) +'</span><p>'+ setValue(escape(JSONValue.description)) +'</p></div><div class="card-action"><h6>Date : '+ JSONValue.created.split(" ")[0] +'</h6></div><div class="card-action"><a onclick="deleteNotes('+ JSONValue.id +')" >Delete</a><a class="modal-trigger" onclick="editNotes(\''+ stringResponse +'\');" >Edit</a></div></div></div></div>';

				parentdiv.appendChild(newdiv);
        	}   
     	} else {
     		alert("No notes available!");	
     	}

}

function makeDataWithDB(tx) {
		tx.executeSql('SELECT * FROM Notes_'+userId, [], querySuccess, errorCB);
}



