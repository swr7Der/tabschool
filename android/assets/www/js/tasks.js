var db = "";
var storage = window.localStorage;	
var userId = JSON.parse(storage.getItem("signInJSON")).id;

$(document).ready( function() {
	db = window.openDatabase("Database", "1.0", "DB", 10000000);
	
	var storage = window.localStorage;	
	
	var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
	
	var fullName = titleCase(receivedDataJSON.first_name + " "+receivedDataJSON.last_name);
	
	var link = "http://vnenterprizes.in/tabschool/uploads/"+receivedDataJSON.profilepic;
			
    $("#profilename").text(fullName);
    document.getElementById("pImage").src = link;
	
	var emailId = receivedDataJSON.email;  
	
	getAllTasks(emailId);
	
	var itemsForSelection = storage.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}
	
});

function getAllTasks(emailId) {
	
	$.ajax({
			
        type: 'POST',
        url: " http://vnenterprizes.in/tabschool/index.php/api/taskmanagerview",
        data: {"email_id": emailId },
        success: function (response) {
        	
        		//alert(JSON.stringify(response));
        		var totalCount = response.length;
        		//alert(totalCount);
        		
        		var parentdiv = document.getElementById('divToAdding');
        
        		while (parentdiv.hasChildNodes()) {
    				parentdiv.removeChild(parentdiv.lastChild);
				}
        		
        		if (totalCount>=1) {
        			for(var i=0; i<totalCount; i++){
        		
        				var newdiv = document.createElement('div');
        			
						newdiv.innerHTML = '<div class="row upward"><div class="col s12 m12"><div class="card white darken-1 overflow"><div class="card-content black-text"><p>'+setValue(escape(response[i].name))+'</p></div><div class="card-action"><a onclick="deleteTask('+response[i].id+')">Delete</a><a onclick="doneTask('+response[i].id+')" class="modal-trigger">Done</a></div></div>';        		
        		
        				parentdiv.appendChild(newdiv);
        			}
        		} else {
        			//alert("No books available!");	
        		}	
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});   	

}

function createTask() {
	
		var receivedDataJSON = JSON.parse(storage.getItem('signInJSON'));
		var emailId = receivedDataJSON.email;
	
		var taskTitle = $("#add_task_title").val(); 
		var dateAddTask = $("#date_add_task").val();
		var ampmAddTask = $('#ampm_add_task').find(":selected").text();
		var hourAddTask = $("#hour_add_task").val();
		var minAddTask = $("#minutes_add_task").val();
		
		if (parseInt(hourAddTask,10) > 12 || parseInt(hourAddTask,10) < 0) {
			alert("Hour must be between 00 and 12");	
		} else if (parseInt(minAddTask,10) > 59 || parseInt(minAddTask,10) < 0) {
			alert("Minutes must be between 00 and 59");	
		} else if (taskTitle.length == 0 || dateAddTask.length == 0 || hourAddTask.length == 0 || minAddTask.length == 0) {
			alert("All fields are necessary!");	
		} else if (hourAddTask.length !=2 || minAddTask.length !=2) {
			alert("Please enter hour and minutes in two digits");	
		} else {
			var taskDetails = taskTitle + " at " + hourAddTask + ":" + minAddTask + " "+ampmAddTask;
		
			$.ajax({
			
        		type: 'POST',
        		url: "http://vnenterprizes.in/tabschool/index.php/api/addtaskmanager",
        		data: {"email_id": emailId, "title": taskDetails, "date":dateAddTask},
        		success: function (response) {
        	
        			alert(response.msg);
        			window.location = "task-manager.html";
            
        		},
        		error: function (errorMessage) {
        			alert("Something went wrong. Please restart the app with active internet connection!");
        		
        		}
        
    		});
			
			
			
		}
		
		//alert(taskTitle+dateAddTask+ampmAddTask+hourAddTask);
}

function doneTask(id) {
	//$("#edit").show();
	alert("Congratulations!! You have completed your Task");
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/deletetaskmanager",
        data: {"task_id":id},
        success: function (response) {
        	
        		alert("Task removed from your task manager");
        		window.location = "task-manager.html";
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
}

function deleteTask(id) {	
	//alert(id);
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/deletetaskmanager",
        data: {"task_id":id},
        success: function (response) {
        	
        		alert(response.msg);
        		window.location = "task-manager.html";
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
}

function titleCase(str) {
     words = str.toLowerCase().split(' ');
    
     for(var i = 0; i < words.length; i++) {
          var letters = words[i].split('');
          letters[0] = letters[0].toUpperCase();
          words[i] = letters.join('');
     }
     return words.join(' ');
}

function setValue(result) {
	return unescape(result);	
}

function addNoteBook() {
	
	var noteBookName = $("#notebook_name").val();	
	if(noteBookName.length ==0){
		alert("Notebook name cannot be empty!");	
	} else {
		
		var savedData  = window.localStorage;
		savedData.setItem("NotebookNames",savedData.getItem("NotebookNames")+ noteBookName+",");
		//alert(savedData.getItem("NotebookNames"));
		var itemsForSelection = savedData.getItem("NotebookNames");
    	var countOfSelection = itemsForSelection.split(",");

		for (var i=0; i<countOfSelection.length - 1; i++) { 
			var select = document.getElementById("notes_option");
			select.options[select.options.length] = new Option(countOfSelection[i], i+1);
			//alert(countOfSelection[i]);
		}   
		
	}
}


function addNotes() {

	var itemsForSelection = storage.getItem("NotebookNames");
   
   if(itemsForSelection == ""){
		alert("Please add at least one notebook for adding notes!");   
   }		
	
	var notesTitle = $("#note_title").val().trim();
	var notesOption = $('#notes_option').find(":selected").attr('value');
	var notesValue = $("#textarea1").val().trim();
	
	//alert(notesTitle+notesOption+notesValue+userId);
	
	if(notesTitle.length == 0 || notesValue.length == 0 || notesOption <=0){
		alert("All fields are necessary!");	
	} else {
		
		$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/addnotes",
        data: {"title": notesTitle, "book_id": notesOption, "user_id":userId, "text":notesValue},
        success: function (response) {
        	
        		alert(response.msg);
        		refreshDB();
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
	}

}


function refreshDB() {

	db.transaction(populateDB, errorCB, successCB);	
	
	$.ajax({
			
        type: 'POST',
        url: "http://vnenterprizes.in/tabschool/index.php/api/viewnotes",
        data: {"user_id": userId},
        success: function (response) {
        	
        		//alert("1 "+JSON.stringify(response));
        		
        		db.transaction(function (tx) { saveDetailsInDB(tx, JSON.stringify(response)); } ,errorCB , successInsertion);

				//alert(response.length);
            
        },
        error: function (errorMessage) {
        		alert("Something went wrong. Please restart the app with active internet connection!");
        		
        }
        
    	});
    	
}


function done(tx) {
	//alert("Positive FULLsuccess ");
}

function populateDB(tx) {
    tx.executeSql('DROP TABLE IF EXISTS Notes_'+userId);
    tx.executeSql('CREATE TABLE IF NOT EXISTS Notes_'+userId+' (ID INTEGER PRIMARY KEY,noteID TEXT, notesJSON TEXT)');
}

function saveDetailsInDB(tx,response){
	//alert(response);
	
	var JSONform = JSON.parse(response);
	var lengthOfArray = JSONform.length;
	
	for(var i=0; i<lengthOfArray; i++){
			var noteId = JSONform[i].id;
			var notesDetails = JSON.stringify(JSONform[i]);
			
			tx.executeSql('INSERT INTO Notes_'+userId+' (noteID, notesJSON) VALUES (?,?)',[noteId, notesDetails],done,errorCB);
	}

}

// Transaction error callback
    
function errorCB(err) {
    //alert("Error processing SQL: "+err.code+" "+err.message);
    alert("Some error has occurred in saving notes!");
}

// Transaction success callback

function successCB() {
  	 //alert("Positive successCB");
}

function successInsertion() {
  	 //alert("Positive successInsertion");
}