

function onSuccess(imageData) {
    //alert(imageData);
    
    console.log(imageData);

    document.getElementById('pImage').src = "data:image/jpeg;base64,"+imageData;
      
}

function onFail(message) {
    alert('Image uploading failed due to internal error!');
}

function changePic() {

	//alert("clicked");
	
	navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
    	destinationType: Camera.DestinationType.DATA_URL,
    	sourceType: Camera.PictureSourceType.PHOTOLIBRARY
		});
		
}